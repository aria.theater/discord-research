# Discord research

A wiki for nuanced research on the Discord service and FOSS projects that integrate with it in interesting ways.

- Undocumented service APIs such as user endpoints
- Novel feature ideas making use of non-standard API calls and event handling
- Quirks of implementation detail of websocket gateway event handling in libraries
- References to FOSS bot features integrating with the above in interesting ways
